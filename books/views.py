from django.shortcuts import render
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponseForbidden, HttpResponse
from django.forms.models import model_to_dict
import requests
from json import loads
from .models import Books

def books(request):
    return render(request, 'books.html')

def get_data(request):
    if request.method == "POST":
        idBuku = request.POST.get('id')
        url = "https://www.googleapis.com/books/v1/volumes?q=" + idBuku
        books_json = requests.get(url).json().get("items")[0]
        try:
            obj = Books.objects.get(id_buku = idBuku)
            obj.jumlah_like += 1
            obj.save()
        except Books.DoesNotExist:
            obj = Books(
                id_buku = idBuku,
                cover = books_json.get("volumeInfo").get("imageLinks").get("smallThumbnail"),
                title = books_json.get("volumeInfo").get("title"),
                authors = books_json.get("volumeInfo").get("authors"),
                jumlah_like = 1
            )
            obj.save()
        
        context = model_to_dict(obj, fields=[field.name for field in obj._meta.fields])
        return JsonResponse({"local_json" : context})


    else:
        url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET.get('keyword')
        books_json = requests.get(url).json()
        lst_books = []

        for book in books_json.get("items"):
            try:
                obj = Books.objects.get(id_buku = book.get("id"))
                temp = {
                    "id" : obj.id_buku,
                    "cover" : obj.cover,
                    "title" : obj.title,
                    "authors" : obj.authors,
                    "likes" : obj.jumlah_like
                }

            except:
                img = book.get('volumeInfo').get('imageLinks', None)
                if(img): img = img.get("smallThumbnail", "")
                else: 
                    print("XXXXXXXXXXXXXX KOSONG WEH XXXXXXXXXXXXXX", )
                    print(book.get("volumeInfo").get("title"))
                    print("XXXXXXXXXXXXXX KOSONG WEH XXXXXXXXXXXXXX", )
                temp = {
                    "id" : book.get("id"),
                    "cover" : img,
                    "title" : book.get("volumeInfo").get("title"),
                    "authors" : book.get("volumeInfo").get("authors"),
                    "likes" : 0
                }
            lst_books.append(temp)
        
        return JsonResponse({"items" : lst_books})