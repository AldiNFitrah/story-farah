from django.urls import path
from . import views

app_name = 'books'

urlpatterns = [
    path('', views.books, name='books'),
    path('books.json', views.get_data, name='json'),
]
