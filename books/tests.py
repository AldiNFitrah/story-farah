from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver

from django.apps import apps
from .apps import BooksConfig
from .views import books
# Create your tests here.

class story8UnitTest(TestCase):
    def test_apps(self):
        self.assertEqual(BooksConfig.name, 'books')
        self.assertEqual(apps.get_app_config('books').name, 'books')

    def test_url_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)
    
    def test_about_function(self):
        self.found = resolve('/books/')
        self.assertEqual(self.found.func, books)
    
    def test_intro_is_exist(self):
        response = Client().get('/books/')
        self.assertIn('<h2>some books you might want to read</h2>', response.content.decode())
    
    def test_search_button_is_exist(self):
        self.response = Client().get('/books/')
        self.assertIn('</button>', self.response.content.decode())


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()

    def test_book_search(self):
        self.browser.get(self.live_server_url + "/books/")
        self.browser.implicitly_wait(10)

        search_box = self.browser.find_element_by_id("search-box")
        search_box.send_keys("Harry Potter")

        search_button = self.browser.find_element_by_id("search-button")
        search_button.click()
        time.sleep(5)

        print(self.browser.page_source)
        text = self.browser.find_element_by_id('output').text
        print(text)
        self.assertIn("Harry Potter", text)
    