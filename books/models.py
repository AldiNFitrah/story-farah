from django.db import models

# Create your models here.

class Books(models.Model):
    id_buku = models.CharField('nama', max_length=200)
    cover = models.CharField('cover', max_length=300, null=True)
    title = models.CharField('title', max_length=200, null=True)
    authors = models.CharField('authors', max_length=200, null=True)
    jumlah_like = models.IntegerField('jumlah like')
    