$(document).ready(() => {
    get_books('Orion');

    $('#search-box').keypress(event => { 
        if(event.which == 13) {
            $('#search-button').click(); 
        }
    });

    $('#search-button').on('click', event => {
        let keyword = $('#search-box').val();
        console.log("keyword")
        if(keyword == '') keyword = 'Orion';
        console.log(keyword)
        get_books(keyword);
    });
    
    function get_books(keyword) {
        $('#output').html(
        `
        <table class="table">
            <thead class="thead-dark">
                <tr>
                <th scope="col">#</th>
                <th scope="col">Cover</th>
                <th scope="col">Title</th>
                <th scope="col">Author(s)</th>
                <th scope="col">Likes</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        `);

        $.ajax({
            type: "GET",
            url: "/books/books.json?keyword=" + keyword,
            success: response => {
                let number = 1;
                response.items.forEach(obj => {
                    var like_icon = $('<i/>').html(obj.likes);
                    like_icon.attr('id', obj.id);
                    like_icon.attr('class', 'fas fa-heart book');
                    like_icon.attr('style', "font-size:40px; color: rgb(45, 89, 128);");
                    var icon = '<i id="' + obj.id + '" class="fas fa-heart book" style="font-size:40px; color: rgb(45, 89, 128);">' + obj.likes + '</i>';
                    $('tbody').append(
                        $('<tr/>').append(
                            $('<td/>').html(number)
                        ).append(
                            $('<td/>').append(
                                $('<img/>').attr('src', obj.cover)
                            )
                        ).append(
                            $('<td/>').html(obj.title)
                        ).append(
                            $('<td/>').html(obj.authors)
                        ).append(
                            $('<td/>').append(
                                like_icon
                            )
                        )
                    );
                    number++;
                });

            },
            error: (a, b) => {
                $('body').append(
                    $('<p/>').html(a)
                ).append(
                    $('<p/>').html(b)
                );
            }
        })
    }
}); 