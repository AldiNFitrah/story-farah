$(document).ready(function() {
  $('.acc-head > button').on('click', function(){
      if($(this).hasClass('active')) {
          $(this).removeClass('active');
          $(this).siblings('.content').slideUp(200);
          $('.acc-head > button i').removeClass("fa-minus").addClass('fa-plus');
      } else {
          $('.acc-head > button i').removeClass('fa-minus').addClass('fa-plus');
          $(this).find('i').removeClass('fa-plus').addClass('fa-minus');
          $('.acc-head > button').removeClass('active');
          $(this).addClass('active');
          $('.content').slideUp(200);
          $(this).siblings('.content').slideDown(200);
      }
  });
  $('.down').click(function (e) {
      var self = $(this),
          item = self.parents('div#accordion'),
          swapWith = item.next();
      item.before(swapWith.detach());
  });
  $('.up').click(function (e) {
      var self = $(this),
          item = self.parents('div#accordion'),
          swapWith = item.prev();
      item.after(swapWith.detach());
  });
});


$("body").addClass(localStorage.getItem("theme"));

setTimeout(function() {
  $("#loader").css("display", "none");
  $("#loader-background").css("display", "none");
  $("#container").css("display", "block");
}, 3000);

$('#dark_theme').hover(
  function() {
    $('body').addClass('dark')
  },
  function() {
    $('body').removeClass('dark')
  }
);

$('#light_theme').hover(
  function() {
    $('body').addClass('light')
  },
  function() {
    $('body').removeClass('light')
  }
);

$(document).ready(function() {
    $("#dark_theme").click(function() {
        localStorage.setItem("theme", "dark-toggled");
        $("body").removeClass("light-toggled");
        $("body").toggleClass("dark-toggled");
    });
});

$(document).ready(function() {
    $("#light_theme").click(function() {
        localStorage.setItem("theme", "light-toggled");
        $("body").removeClass("dark-toggled");
        $("body").toggleClass("light-toggled");
    });
});
