from django.db import models

# Create your models here.

class Status(models.Model):
    nama = models.CharField('nama', max_length=50)
    pesan = models.TextField('pesan', max_length=200)
    color = models.CharField('color', max_length=8, null=True)